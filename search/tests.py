from django.test import TestCase


# Create your tests here.


class SearchTestCase(TestCase):
    fixtures = ['one_book.json']

    def test_search_title(self):
        phrase = 'ارباب'
        self.response = self.client.get('/search/?phrase=' + str(phrase))
        self.assertEqual(self.response.context['books'][0].pk, 1)
