from django.shortcuts import render
from django.db.models import Q

from books.models import Book


# Create your views here.
def search(request):
    phrase = request.GET['phrase']
    query = None
    for word in phrase.split():
        if query is None:
            query = Q(title__contains=word) | Q(author__contains=word)  # | Q(isbn=word)
        else:
            query = query | Q(title__contains=word) | Q(author__contains=word)  # | Q(isbn=word)
    results = Book.objects.filter(query)
    return render(request, 'ketabkhaar/index.html', {
        'title': 'نتایج جستجو برای ' + phrase,
        'books': results
    })
