from rest_framework import generics, mixins


from .serializers import BookCategoryRetrieveSerializer, BookReviewSerializer
from ..models import Book, BookReview, BookCategory


class BookCategoryRetrieve(generics.RetrieveAPIView):

    serializer_class = BookCategoryRetrieveSerializer
    queryset = BookCategory.objects.all()


class BookReviewRetrieve(generics.RetrieveAPIView):

    serializer_class = BookReviewSerializer
    queryset = BookReview.objects.all()