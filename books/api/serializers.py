from rest_framework import serializers

from ..models import Book, BookRating, BookReview, BookCategory
from accounts.models import UserProfile


class MultiSerializerViewSetMixin(object):
    def get_serializer_class(self):

        serializer_action_classes = getattr(self, 'serializer_action_classes', None)
        if serializer_action_classes is not None:
            serializer_class = serializer_action_classes.get(self.action, None)
            if serializer_class is not None:
                return serializer_class

        return super(MultiSerializerViewSetMixin, self).get_serializer_class()


class BookRatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = BookRating
        fields = ('id', 'rate', 'user_profile')
        read_only_fields = ('id',)

    def perform_create(self, serializer):
        serializer.save(user_profile=UserProfile.objects.filter(user=self.request.user).first())

    def create(self, validated_data):
        instance, _ = BookRating.objects.get_or_create(**validated_data)
        return instance


class BookDetailSerializer(serializers.ModelSerializer):
    ratings = BookRatingSerializer(source='bookrating_set', many=True)

    class Meta:
        model = Book
        fields = ('id', 'title', 'get_avg_rating', 'ratings', 'author', 'isbn', 'translator', 'description',
                  'page_count', 'price', 'publisher', 'pic', 'category', 'owner')
        read_only_fields = ('id', 'get_avg_rating')
        depth = 1


class BookListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Book
        fields = ('id', 'title', 'get_avg_rating')
        read_only_fields = ('id', 'get_avg_rating')


class BookCategoryRetrieveSerializer(serializers.ModelSerializer):
    books = BookListSerializer(source='book_set', many=True)

    class Meta:
        model = BookCategory
        fields = ('id', 'title', 'books')
        read_only_fields = ('id', 'books')


class BookCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = ('id', 'title', 'author', 'isbn', 'translator', 'description',
                  'page_count', 'price', 'publisher', 'pic', 'category', 'owner')
        read_only_fields = ('id',)

    def perform_create(self, serializer):
        serializer.save(owner=UserProfile.objects.filter(user=self.request.user).first())


class BookReviewSerializer(serializers.ModelSerializer):

    class Meta:
        model = BookReview
        fields = ('id', 'text', 'user_profile', 'date', 'book')
        read_only_fields = ('id',)

    def perform_create(self, serializer):
        serializer.save(user_profile=UserProfile.objects.filter(user=self.request.user).first())
