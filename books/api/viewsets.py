from rest_framework import viewsets


from .serializers import BookListSerializer, MultiSerializerViewSetMixin, BookCreateSerializer, BookDetailSerializer,\
    BookReviewSerializer, BookRatingSerializer
from ..models import Book, BookReview, BookRating
from accounts.models import UserProfile


class BookOwnerViewSet(MultiSerializerViewSetMixin,
                       viewsets.ModelViewSet):

    serializer_class = BookListSerializer
    serializer_action_classes = {
        'create': BookCreateSerializer,
        'retrieve': BookDetailSerializer
    }
    lookup_field = 'id'

    def get_queryset(self):
        user_profile = UserProfile.objects.filter(user=self.request.user).first()
        queryset = Book.objects.filter(owner=user_profile)
        return queryset


class BookReviewOwnerViewSet(viewsets.ModelViewSet):

    serializer_class = BookReviewSerializer
    lookup_field = 'id'

    def get_queryset(self):
        user_profile = UserProfile.objects.filter(user=self.request.user).first()
        queryset = BookReview.objects.filter(user_profile=user_profile)
        return queryset


class BookRatingOwnerViewSet(viewsets.ModelViewSet):

    serializer_class = BookRatingSerializer
    lookup_field = 'id'

    def get_queryset(self):
        user_profile = UserProfile.objects.filter(user=self.request.user).first()
        queryset = BookRating.objects.filter(user_profile=user_profile)
        return queryset