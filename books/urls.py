from django.conf.urls import url

from books import views

app_name = 'books'
urlpatterns = [
    url(r'^categories/(?P<category_name>.+)/$', views.get_category, name='get_category'),
    url(r'^(?P<book_id>[0-9]+)/$', views.book_details, name='book_details'),
    url(r'^(?P<book_id>[0-9]+)/review/add/$', views.add_book_review, name='add_book_review'),
    url(r'^(?P<book_id>[0-9]+)/rate/add/$', views.rate_book, name='rate_book'),
    url(r'^vending/$', views.vending, name='vending'),
    url(r'^giveback/$', views.give_back, name='give_back'),
    url(r'^accept/$', views.accept, name='accept'),
    url(r'^reject/$', views.reject, name='reject'),
    url(r'^cancel/$', views.cancel, name='cancel'),
    url(r'^new/$', views.new_book, name='new_book'),
]
