from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404
from django.urls import reverse

from accounts.models import UserProfile
from books.models import Book, BookReview, BookRating, BorrowRequest
from .forms import NewBookForm


def book_details(request, book_id):
    book = get_object_or_404(Book, pk=book_id)
    all_bookratings = book.bookrating_set.all()
    user = request.user
    user_profile = None
    if user.is_authenticated:
        user_profile = get_object_or_404(UserProfile, user=user)
        book_rate = user_profile.bookrating_set.filter(book=book)
        if book_rate.count() > 0:
            user_book_rate = book_rate[0].rate
        else:
            user_book_rate = 0
    else:
        user_book_rate = 0
    if all_bookratings.count() == 0:
        book_rate_avg = 0
    else:
        book_rate_avg = book.get_avg_rating

    return render(request, 'books/book-detail.html', {
        'book': book,
        'book_rate': book_rate_avg,
        'user_book_rate': user_book_rate,
        'user_profile': user_profile,
    })


def add_book_review(request, book_id):
    if request.method == 'POST':
        if request.POST.get('text').strip() == '':
            return JsonResponse({'status': 'failure'})
        book = get_object_or_404(Book, pk=book_id)
        user_profile = get_object_or_404(UserProfile, user=request.user)
        book_review = BookReview(user_profile=user_profile,
                                 book=book,
                                 text=request.POST.get('text'))
        book_review.save()
        return JsonResponse({'status': 'ok', 'url': reverse('books:book_details', args=(book_id,))})
    else:
        return JsonResponse({'status': 'failure'})


def rate_book(request, book_id):
    if request.method == 'POST':
        rate = request.POST.get('rate', -1)
        if rate == -1:
            return JsonResponse({'status': 'failure'})
        book = get_object_or_404(Book, pk=book_id)
        user_profile = get_object_or_404(UserProfile, user=request.user)
        book_rate, created = BookRating.objects.get_or_create(
            book=book,
            user_profile=user_profile
        )
        book_rate.rate = rate
        book_rate.save()
        return JsonResponse({'status': 'ok', 'url': reverse('books:book_details', args=(book_id,))})
    else:
        return JsonResponse({'status': 'failure'})


def get_category(request, category_name):
    results = Book.objects.filter(category__title=category_name)

    return render(request, 'ketabkhaar/index.html', {
        'title': 'کتاب‌های ' + category_name,
        'books': results,
        'category': category_name
    })


@login_required
def vending(request):
    borrower = UserProfile.objects.get(user=request.user)
    book_id = request.POST['book_id']
    book = Book.objects.get(id=book_id)
    req = BorrowRequest()
    req.book = book
    req.borrower = borrower
    req.save()
    return render(request, 'books/borrowedMessage.html', {
        'book': book,
        'is_borrowing': True
    })


@login_required
def give_back(request):
    book_id = request.POST['book_id']
    book = Book.objects.get(id=book_id)
    book.borrower = None
    book.save()
    return render(request, 'books/borrowedMessage.html', {
        'book': book,
        'is_borrowing': False
    })


@login_required
def reject(request):
    request_id = request.POST['request_id']
    borrow_request = BorrowRequest.objects.get(id=request_id)
    borrow_request.status = 'r'
    borrow_request.save()

    return JsonResponse({
        'status': 'ok'
    })


@login_required
def accept(request):
    request_id = request.POST['request_id']
    borrow_request = BorrowRequest.objects.get(id=request_id)
    borrow_request.status = 'a'
    borrow_request.save()

    borrow_request.book.borrower = borrow_request.borrower
    borrow_request.book.save()

    borrow_request.borrower.credit -= borrow_request.book.price
    borrow_request.borrower.save()

    return JsonResponse({
        'status': 'ok'
    })


@login_required
def cancel(request):
    request_id = request.POST['request_id']
    borrow_request = BorrowRequest.objects.get(id=request_id)
    borrow_request.delete()

    return JsonResponse({
        'status': 'ok'
    })


@login_required
def new_book(request):
    created = False

    if request.method == 'POST':
        new_book_form = NewBookForm(request.POST, request.FILES)
        if new_book_form.is_valid():
            created = True
            user_profile = UserProfile.objects.get(user=request.user)

            the_book = new_book_form.save(commit=False)
            the_book.owner = user_profile
            the_book.save()
    else:
        new_book_form = NewBookForm()

    return render(request, 'books/new.html', {
        'created': created,
        'new_book_form': new_book_form
    })
