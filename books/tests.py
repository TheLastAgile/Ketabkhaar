from django.test import TestCase, Client
from books.models import Book
import json


# Create your tests here.


class BookViewTestCase(TestCase):
    serialized_rollback = True
    fixtures = ['one_book.json']

    def setUp(self):
        self.response = self.client.get('/books/1', follow=True)

    def test_access(self):
        self.assertEqual(self.response.status_code, 200)

    def test_book_context(self):
        self.assertTrue('book' in self.response.context)
        self.assertEqual(1, self.response.context['book'].pk)

    def test_book_content(self):
        self.assertTrue('ارباب'.encode() in self.response.content)
        self.assertTrue('حلقه'.encode() in self.response.content)
        self.assertTrue('تالکین'.encode() in self.response.content)


class BookModelTestCase(TestCase):
    def setUp(self):
        self.book = Book.objects.create(title='title',
                                        author='author',
                                        description='description',
                                        publisher='publisher')

    def test_min_required(self):
        self.assertTrue(isinstance(self.book, Book))

    def test_str(self):
        self.assertEqual(str(self.book), 'title - publisher')


class BookCategoryTestCase(TestCase):
    fixtures = ['one_book.json']

    def setUp(self):
        self.response = self.client.get('/books/categories/فانتزی', follow=True)

    def test_get_category(self):
        self.assertTrue(len(self.response.context['books']) == 1)
        self.assertTrue(self.response.context['books'][0].pk == 1)
        self.assertTrue(self.response.context['category'] == 'فانتزی')


class BookReviewTestCase(TestCase):
    fixtures = ['one_book.json']

    def test_add_review(self):
        self.client.login(username='test', password='123qweasd')
        self.response = self.client.post(path='/books/1/review/add/', data={'text': 'کتاب خوبی است'})
        self.response = json.loads(self.response.content)
        self.assertEqual(self.response['status'], 'ok')

    def test_add_review_missing_text(self):
        self.response = self.client.post('/books/1/review/add', {}, follow=True)
        self.client.login(username='test', password='123qweasd')
        self.response = json.loads(self.response.content)
        self.assertEqual(self.response['status'], 'failure')

    def test_get_review(self):
        self.client.login(username='test', password='123qweasd')
        self.response = self.client.post(path='/books/1/review/add/', data={'text': 'کتاب خوبی است'})
        self.response = self.client.post(path='/books/1/review/add/', data={'text': 'کتاب عالی است'})

        self.response = self.client.get('/books/1', follow=True)
        self.assertEqual(len(self.response.context['book'].bookreview_set.all()), 2)


class BookRating(TestCase):
    fixtures = ['one_book.json']

    def test_add_rating(self):
        self.client.login(username='test', password='123qweasd')
        self.response = self.client.post(path='/books/1/rate/add/', data={'rate': 5})
        self.response = json.loads(self.response.content)
        self.assertEqual(self.response['status'], 'ok')

    def test_get_avg_rating(self):
        self.client.login(username='test', password='123qweasd')
        self.response = self.client.post(path='/books/1/rate/add/', data={'rate': 5})
        self.client.login(username='test2', password='123qweasd')
        self.response = self.client.post(path='/books/1/rate/add/', data={'rate': 3})

        self.response = self.client.get('/books/1', follow=True)
        self.assertEqual(self.response.context['book_rate'], 4)
