from django.forms import ModelForm
from .models import Book

class NewBookForm(ModelForm):
    class Meta:
        model = Book
        exclude = ['owner', 'borrower']
        labels = {
            'isbn': 'شماره شابک',
            'title': 'نام کتاب',
            'author': 'نویسنده',
            'translator': 'مترجم',
            'description': 'توضیحات',
            'page_count': 'تعداد صفحات',
            'publisher': 'ناشر',
            'price': 'قیمت اجاره',
            'pic': 'تصویر جلد',
            'category': 'سبک'
        }