from django.conf.urls import url

from accounts import views

app_name = 'accounts'
urlpatterns = [
    url(r'^login/$', views.user_login, name='user_login'),
    url(r'^login-phonenumber/$', views.user_login_phone_number, name='user_login_phone_number'),
    url(r'^enter-confirmation-code/$', views.user_enter_confirmation_code, name='user_enter_confirmation_code'),
    url(r'^register/$', views.user_register, name='user_register'),
    url(r'^logout/$', views.user_logout, name='user_logout'),
    url(r'^profile/(?P<user_id>[0-9]+)/$', views.view_profile, name='user_profile'),
    url(r'^profile/edit/$', views.edit_profile, name='edit_profile'),
    url(r'^credit/increase/$', views.increase_credit, name='increase_credit'),
]
