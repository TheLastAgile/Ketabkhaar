from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render, redirect, get_object_or_404

from accounts.forms import UserRegisterForm, UserLoginPhoneNumber, EnterConfirmationCode
from accounts.models import UserProfile, UserCredential
from accounts.utils import *
from books.models import Book, BookReview, BookRating, BorrowRequest


def user_login(request):
    if request.method == 'GET':
        return render(request, 'accounts/login.html',
                      {'login_phone_number_form': UserLoginPhoneNumber()})


def user_login_phone_number(request):
    if request.method == 'POST':
        form = UserLoginPhoneNumber(request.POST)
        if form.is_valid():
            phone_number = form.cleaned_data.get('phone_number')
            confirmation_code = generate_confirmation_code(4)
            user_credential = UserCredential.objects.filter(phone_number=phone_number).first()
            if user_credential:
                user_credential.confirmation_code = confirmation_code
            else:
                user_credential = UserCredential(phone_number=phone_number, confirmation_code=confirmation_code)
            user_credential.save()
            send_confirmation_code(phone_number, confirmation_code)
            return render(request, 'accounts/enter-confirmation.html',
                          {'phone_number': phone_number, 'confirmation_code_form': EnterConfirmationCode()})


def user_enter_confirmation_code(request):
    if request.method == 'POST':
        form = EnterConfirmationCode(request.POST)
        if form.is_valid():
            confirmation_code = request.POST['confirmation_code']
            phone_number = request.POST['phone_number']
            user_credential = UserCredential.objects.filter(phone_number=phone_number,
                                                            confirmation_code=confirmation_code).first()
            if user_credential:
                user_profile = UserProfile.objects.filter(phone_number=phone_number).first()
                if user_profile:
                    login(request, user_profile.user)
                    return render(request, 'accounts/login.html', {'login': True})
                else:
                    return render(request, 'accounts/register.html',
                                  {'register': False, 'register_form': UserRegisterForm(),
                                   'phone_number': phone_number})
            else:
                return render(request, 'accounts/enter-confirmation.html',
                              {'error': 'کد وارد شده صحیح نیست.', 'confirmation_code_form': form,
                               'phone_number': phone_number})


def user_register(request):
    if request.method == 'POST':
        phone_number = request.POST['phone_number']
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            user = User(
                username=form.cleaned_data.get('username'),
                first_name=form.cleaned_data.get('firstname'),
                last_name=form.cleaned_data.get('lastname'),
                email=form.cleaned_data.get('email'),
            )

            user.set_password(form.cleaned_data.get('password'))

            user.save()

            try:
                referrer = UserProfile.objects.get(referral_code=form.cleaned_data.get('referral_code'))
                referrer.credit += 5000
                referrer.save()
            except:
                referrer = None

            user_profile = UserProfile(
                user=user,
                referrer=referrer,
                phone_number=phone_number
            )

            if referrer is not None:
                user_profile.credit = 3000

            user_profile.save()

            login(request, user)
            return render(request, 'accounts/register.html',
                          {'register': True, 'register_form': form, 'phone_number': phone_number})
        else:
            return render(request, 'accounts/register.html',
                          {'register': False, 'register_form': form, 'phone_number': phone_number})


def user_logout(request):
    logout(request)
    return redirect('home_page')


def view_profile(request, user_id):
    user_profile = get_object_or_404(UserProfile, user__id=user_id)
    if request.user == user_profile.user:
        is_owner = True
        borrow_requests = BorrowRequest.objects.filter(book__owner=user_profile)
        borrows = BorrowRequest.objects.filter(borrower=user_profile)
    else:
        is_owner = False
        borrow_requests = None
        borrows = None

    rated = BookRating.objects.filter(user_profile=user_profile)
    books = Book.objects.filter(owner=user_profile)
    reviews = BookReview.objects.filter(user_profile=user_profile)

    return render(request, 'accounts/profile.html', {
        'user_profile': user_profile,
        'books': books,
        'is_owner': is_owner,
        'rated': rated,
        'reviews': reviews,
        'active_tab': 2,
        'borrow_requests': borrow_requests,
        'borrows': borrows
    })


@login_required
def edit_profile(request):
    user_profile = get_object_or_404(UserProfile, user=request.user)
    if request.method == 'POST':
        if request.POST['action'] == 'other':
            user_profile.user.first_name = request.POST['name']
            user_profile.user.last_name = request.POST['familyName']
            user_profile.user.email = request.POST['email']
            user_profile.birthday = request.POST['birthday']
            user_profile.user.save()
            user_profile.save()
        else:
            pass

    return render(request, 'accounts/settings.html', {
        'user_profile': user_profile
    })


@login_required
def increase_credit(request):
    if request.method == 'POST':
        submitted = True
        user_profile = UserProfile.objects.get(user=request.user)
        user_profile.credit += int(request.POST['amount'])
        user_profile.save()
    else:
        submitted = False

    return render(request, 'accounts/increase.html', {
        'submitted': submitted
    })
