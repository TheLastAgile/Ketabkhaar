from django.test import TestCase
from accounts.models import UserProfile
from django.contrib.auth.models import User
from datetime import date
from accounts.forms import UserRegisterForm


# Create your tests here.

class AccountCreateTestCase(TestCase):
    def setUp(self):
        user = User.objects.create_user('test', 'test@test.com', '123')

    def test_userProfile_default(self):
        ''' check UserProfile and default values '''
        user = User.objects.get(username='test')
        UserProfile.objects.create(user=user)

        userProfile = UserProfile.objects.get(user=user)
        self.assertEqual(userProfile.user.username, 'test')
        self.assertEqual(userProfile.birthday, None)
        self.assertEqual(userProfile.avatar, 'static/site-media/photos/profiles/matthew.png')
        self.assertEqual(str(userProfile), 'test')

    def test_userProfile_setValue(self):
        ''' check UserProfile set values '''
        user = User.objects.get(username='test')
        birthday = date(1995, 6, 24)
        avatar = 'static/site-media/photos/profiles/test.png'
        UserProfile.objects.create(user=user, birthday=birthday, avatar=avatar)

        user = User.objects.get(username='test')
        userProfile = UserProfile.objects.get(user=user)
        self.assertEqual(userProfile.user.username, 'test')
        self.assertEqual(userProfile.birthday, birthday)
        self.assertEqual(userProfile.avatar, avatar)


class AccountLoginTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create_user('test', 'test@test.com', '123')

    # Register validate

    def test_register_form_validate(self):
        form = UserRegisterForm(initial={
            'username': 'test',
            'firstname': 'mahdi',
        })
        self.assertFalse(form.is_valid())

    def test_register_form_match_password(self):
        form = UserRegisterForm(initial={
            'username': 'test',
            'firstname': 'testname',
            'lastname': 'testlast',
            'password': 'pass',
            'password_r': 'pass1'
        })
        self.assertFalse(form.is_valid())



class ProfileTestCase(TestCase):
    fixtures = ['one_book.json']

    def test_get_profile(self):
        self.client.login(username='test', password='123qweasd')
        self.response = self.client.get('/accounts/profile/1/')
        self.assertEqual(self.response.status_code, 200)
        self.assertEqual(len(self.response.context['books']), 1)

    def test_edit_profile(self):
        self.client.login(username='test', password='123qweasd')
        self.response = self.client.post('/accounts/profile/edit/', data={
            'action': 'other',
            'name': 'mahdi',
            'familyName': 'mahdavi',
            'email': 'mahdi@mahdi.com',
            'birthday': '1995-01-16'})

        self.response = self.client.get('/accounts/profile/1/')
        self.assertEqual(self.response.context['user_profile'].user.first_name, 'mahdi')
        self.assertEqual(self.response.context['user_profile'].user.last_name, 'mahdavi')
