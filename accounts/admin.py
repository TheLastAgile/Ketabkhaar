from django.contrib import admin

from .models import UserProfile, UserCredential

admin.site.register(UserProfile)
admin.site.register(UserCredential)
