def convert_phone_number_to_standard_format(phone_number):
    phone_number = phone_number.replace('+', '')
    converted_phone_number = ''
    index_of_98 = phone_number.index('98')
    if index_of_98 < 0 or index_of_98 > 0:
        if phone_number.index('0') == 0:
            converted_phone_number = '98' + phone_number[0:1]
        else:
            converted_phone_number = '98' + phone_number[0:1]

    elif phone_number.index('0') == 0:
        converted_phone_number = phone_number[0:1]
    else:
        converted_phone_number = phone_number
    return converted_phone_number


