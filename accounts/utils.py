import math, random

from kavenegar import KavenegarAPI
from accounts.configs import *


def generate_confirmation_code(length):
    chars = '0123456789'
    code = ''
    for i in range(0, length):
        rnum = math.floor(random.random() * len(chars))
        code += chars[rnum: rnum + 1]
    return code


def send_confirmation_code(phone_number, confirmation_code):
    message = 'کتاب‌خوار\nکد تأیید شما: '
    message += str(confirmation_code)
    send_sms(phone_number, message)


def send_sms(phone_number, message):
    api = KavenegarAPI(KAVE_NEGAR_CONFIGS['API_KEY'])
    params = {
        'sender': KAVE_NEGAR_CONFIGS['SENDER'],
        'receptor': phone_number,
        'message': message
    }
    api.sms_send(params)
