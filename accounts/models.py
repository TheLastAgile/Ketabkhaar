from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.db import models

import random, string


def generate_referral():
    while True:
        code = 'BINO_' + ''.join(random.choices(string.ascii_letters + string.digits, k=10))

        if UserProfile.objects.filter(referral_code=code).count() == 0:
            break

    return code


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    birthday = models.DateField(null=True, blank=True)
    avatar = models.ImageField(upload_to='static/site-media/photos/profiles/', blank=True, null=True,
                               default='static/site-media/photos/profiles/matthew.png')
    credit = models.IntegerField(default=0)
    referral_code = models.CharField(max_length=15, default=generate_referral)
    referrer = models.ForeignKey('UserProfile', null=True, blank=True, on_delete=models.DO_NOTHING)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex], max_length=17, blank=True)

    def __str__(self):
        return str(self.user)


class UserCredential(models.Model):
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex], max_length=17, blank=True)
    confirmation_code = models.IntegerField()
