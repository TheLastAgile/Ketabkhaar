from rest_framework import serializers

from accounts.models import UserProfile


class UserProfileRetrieveSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserProfile
        fields = ('id', 'birthday', 'avatar', 'referral_code', 'user')
        read_only_fields = ('id',)


class UserProfileCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserProfile
        fields = ('id', 'birthday', 'avatar', 'referral_code', 'user', 'credit', 'referrer')
        read_only_fields = ('id',)