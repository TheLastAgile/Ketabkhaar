from rest_framework import generics


from .serializers import UserProfileRetrieveSerializer
from ..models import UserProfile


class ProfileRetrieve(generics.RetrieveAPIView):

    serializer_class = UserProfileRetrieveSerializer
    queryset = UserProfile.objects.all()