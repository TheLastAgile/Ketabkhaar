from rest_framework import viewsets

from .serializers import UserProfileCreateSerializer
from ..models import UserProfile


class UserProfileOwnerViewSet(viewsets.ModelViewSet):

    serializer_class = UserProfileCreateSerializer
    lookup_field = 'id'

    def get_queryset(self):
        queryset = UserProfile.objects.filter(user=self.request.user).first()
        return queryset
