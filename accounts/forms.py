from django import forms
from django.contrib.auth import authenticate
from django.core.validators import RegexValidator


def convert_phone_number_to_standard_format(phone_number):
    # phone_number = phone_number.replace('+', '')
    # converted_phone_number = ''
    # index_of_98 = phone_number.index('98')
    # if index_of_98 < 0 or index_of_98 > 0:
    #     if phone_number.index('0') == 0:
    #         converted_phone_number = '98' + phone_number[0:1]
    #     else:
    #         converted_phone_number = '98' + phone_number[0:1]
    #
    # elif phone_number.index('0') == 0:
    #     converted_phone_number = phone_number[0:1]
    # else:
    #     converted_phone_number = phone_number
    # return converted_phone_number
    return phone_number


class UserRegisterForm(forms.Form):
    username = forms.CharField(label='نام کاربری', max_length=64, required=True)
    firstname = forms.CharField(label='نام', max_length=64, required=True)
    lastname = forms.CharField(label='نام خانوادگی', max_length=128, required=True)
    email = forms.EmailField(label='ایمیل', required=True)
    referral_code = forms.CharField(label='کد معرف', max_length=15, required=False)




class UserLoginPhoneNumber(forms.Form):
    phone_number = forms.CharField(label='شماره تلفن', max_length=13, required=True)

    def clean_phone_number(self):
        phone_number = self.cleaned_data.get('phone_number')
        converted_phone_number = convert_phone_number_to_standard_format(phone_number)
        return converted_phone_number


class EnterConfirmationCode(forms.Form):
    confirmation_code = forms.IntegerField(label='کد تأیید', required=True)
