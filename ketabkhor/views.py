from django.shortcuts import render
from books.models import Book


def home_page(request):
    return render(request, "ketabkhaar/index.html", {
        'title': 'کتاب‌های پربازدید',
        'books': Book.objects.all()  # TODO most viewed
    })
