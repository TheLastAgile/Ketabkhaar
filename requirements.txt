kavenegar
Django==2.0
olefile==0.44
Pillow==4.3.0
pytz==2017.3
django-rest-swagger==2.1.2
djangorestframework==3.7.7
djangorestframework-jwt==1.11.0
