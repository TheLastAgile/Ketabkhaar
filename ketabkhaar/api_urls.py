from django.conf.urls import url, include
from rest_framework import routers
from books.api import viewsets as books_viewsets
from accounts.api import viewsets as accounts_viewsets
from rest_framework_swagger.views import get_swagger_view
from books.api import views as books_view
from accounts.api import views as accounts_view
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token

router = routers.DefaultRouter()
router.register(r'me/books', books_viewsets.BookOwnerViewSet, base_name='my_books')
router.register(r'me/bookreviews', books_viewsets.BookReviewOwnerViewSet, base_name='my_book_review')
router.register(r'me/bookrating', books_viewsets.BookRatingOwnerViewSet, base_name='my_book_rating')
router.register(r'me/profile', accounts_viewsets.UserProfileOwnerViewSet, base_name='my_book_rating')

schema_view = get_swagger_view(title='Pastebin API')

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^category/(?P<pk>[0-9]+)/$', books_view.BookCategoryRetrieve.as_view(), name='category'),
    url(r'^bookreviews/(?P<pk>[0-9]+)/$', books_view.BookReviewRetrieve.as_view()),
    url(r'^profile/(?P<pk>[0-9]+)/$', accounts_view.ProfileRetrieve.as_view()),
    url(r'^docs/', schema_view),
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^api-token-refresh/', refresh_jwt_token),
    url(r'^api-token-verify/', verify_jwt_token),

]